# Soundrome
Jetbrains PyChamps Hackathon 2020

# Third Party API's needed

1. YouTube Data API
2. SoundCloud API

# Steps to launch the frontend platform

1. yarn
2. npm run start

# Steps to run ASYNC REST API
You will need to setup redis, PSQL in order to run the API. Configs for these are in /src/soundrome/api/app.py

1. sudo make
2. bin/api

